extends Node2D

# Carrega Player_inicial
const PLAYER = preload("res://Player_inicial/Player.tscn")
const MAX_PLAYER = 4
var player_pos
var player_rot

# Contadores
var numPlayer = 0
var dead = 0

func _ready():
	randomize()

func _create_player():
	var vector = Vector2(rand_range(50, 961), rand_range(50, 571))
	var GrabbedInstance = PLAYER.instance()
	
	player_pos = get_child(numPlayer).position
	player_rot = get_child(numPlayer).get_rotation()
		
	GrabbedInstance.position = player_pos
	GrabbedInstance.set_rotation(player_rot)
	add_child(GrabbedInstance)
	numPlayer += 1
	print("PLAYER CREATED")
	
	# Recebe o sinal "dead"
	GrabbedInstance.connect("died", self, "_is_over")

func _input(event):
	if event.is_action_pressed("new_player") and numPlayer < MAX_PLAYER:
		_create_player()

func _is_over():
	dead += 1
	print("DEAD")
	if dead == numPlayer - 1:
		print("GAME OVER")
		get_tree().paused = true
		
