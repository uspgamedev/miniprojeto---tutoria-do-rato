extends TextureButton

var map = null
var icon = null

func setup(map, icon):
	assert(map != null)
	self.map = map

	if icon != null:
		self.icon = icon

func _ready():
	if icon != null:
		self.set_normal_texture(load(icon))

func _on_Map_Button_pressed():
	assert(map != null)

	var error = get_tree().change_scene(map)

	if error:
		print(error)
		get_tree().quit()
