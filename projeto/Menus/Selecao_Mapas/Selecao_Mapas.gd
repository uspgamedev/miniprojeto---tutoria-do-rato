extends Panel

#Constants
const MAP_PATH = "res://mapas/"
const MAP_BUTTON_RESOURCE = preload("res://Menus/Selecao_Mapas/Components/Map_Button/Map_Button.tscn")

#Functions
func _ready():
	var dir = Directory.new()

	#Start to search the directory
	assert(dir.open(MAP_PATH) == OK)
	dir.list_dir_begin(true,true)
	var file_name = dir.get_next()

	#Get all folder inside MAP_PATH
	while (file_name != ""):
		if dir.current_is_dir():
			print("Found directory: " + file_name)
			var map_icon
			var map_path

			#Open the folder we just found
			var map_folder = Directory.new()
			assert(map_folder.open("%s/%s" % [MAP_PATH, file_name] ) == OK)
			map_folder.list_dir_begin(true,true)

			var map_file_name = map_folder.get_next()
			while(map_file_name != ""):

				#Find the .tscn and .png files inside the map folder
				if map_file_name.match("*.tscn"):
					map_path = "%s/%s/%s" % [MAP_PATH, file_name, map_file_name]
				elif map_file_name.match("*.png"):
					map_icon = "%s/%s/%s" % [MAP_PATH, file_name, map_file_name]

				map_file_name = map_folder.get_next()

			map_folder.list_dir_end()

			#Create the new map_list_item
			var mapas_item = MAP_BUTTON_RESOURCE.instance()
			mapas_item.setup(map_path, map_icon)
			$Mapas.add_child(mapas_item)

		file_name = dir.get_next()

	dir.list_dir_end()
