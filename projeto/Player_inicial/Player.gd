extends KinematicBody2D

#Constantes
const ROTATION_SPEED = 1
const ROTATION_LIMIT = 5
const SPEED_LIMIT = 300
onready var RADIUS = get_node("Collision_bola").shape.radius

#Velocidade do personagem
var speed  = 0

#Estado do personagem
var rotation_direction = 0
enum turning {none, right, left}
enum direction {none, up, down}
var state = direction.none
var rotation_state = turning.none
var has_ball = true

#Vida do personagem
const MAX_HEALTH = 100
var vida = 100
signal died

#Atributos
var move_direction = Vector2(0, 0)

#Bola instanciada no personagem
onready var bola = $bola
#Função auxiliar para pegar a distancia da bola
func ball_dist():
	return -(8*(bola.scale.x+1)+bola.get_radius())

#Sinal e timer
signal dead
var timer = Timer.new()

func _ready():
	timer = Timer.new()
	timer.wait_time = 1
	add_child(timer)
	timer.connect("timeout", self, "_toma_dano", [10])
	timer.start()

func _input(event):
	#Actions pressed
	if event.is_action_pressed("ui_right"):
		rotation_state = turning.right

	elif event.is_action_pressed("ui_left"):
		rotation_state = turning.left

	elif event.is_action_pressed("ui_up"):
		state = direction.up

	elif event.is_action_pressed("ui_down"):
		state = direction.down

	elif event.is_action_pressed("ui_select") and has_ball:
		has_ball = false

		#desabilitando colisão da bola
		$"Collision_bola".disabled = true
		#removendo ligação do player com a bola
		#a fim de não possibilitar movimento
		#conjunto após lançamento
		var pai = self.get_parent()
		var ball_pos = bola.global_position
		self.remove_child(bola)
		bola.position = ball_pos
		pai.add_child(bola)
		#chamando função de lançamento da bola
		#e transformando ângulo em vetor
		# "1.5" é necessário pois embora comece
		#de um ângulo 0, o player não inicia virado
		#para a direita, mas para cima
		var direction = Vector2(cos(rotation-1.5), sin(rotation-1.5))
		bola.launch_ball(direction)
		#ligando o timer
		$Timer.start()

	#Actions released
	elif event.is_action_released("ui_right"):
		rotation_state = turning.none
	elif event.is_action_released("ui_left"):
		rotation_state = turning.none
	elif event.is_action_released("ui_up"):
		state = direction.none
	elif event.is_action_released("ui_down"):
		state = direction.none

#warning-ignore:unused_argument
func _process(delta):
	#Lida com os estados
	if rotation_state == turning.right:
		if rotation_direction < ROTATION_LIMIT:
			rotation_direction += 1
	if rotation_state == turning.left:
		if rotation_direction > -ROTATION_LIMIT:
			rotation_direction -= 1
	if state == direction.up:
		if speed > -SPEED_LIMIT:
			speed -= 10
	if state == direction.down:
		if speed < SPEED_LIMIT:
			speed += 10
	if rotation_state == turning.none:
		rotation_direction = 0

func _physics_process(delta):
	#Rotação
	rotation += rotation_direction * ROTATION_SPEED * delta

	#Lida com os estados
	if state == direction.none:
		if speed > 0 and move_direction.y > 0:
			speed -= 2
			move_direction.y = speed
		elif speed < 0 and move_direction.y < 0:
			speed += 2
			move_direction.y = speed
	elif state == direction.down:
		move_direction.y = speed
	elif state == direction.up:
		move_direction.y = speed

	#Movimenta o personagem
#warning-ignore:return_value_discarded
	move_and_slide(move_direction.rotated(rotation))

#Coleção de entulho
func _pegou_entulho(tamanho):
	if $bola._entulho_pego(tamanho):
		bola.set_position(Vector2(0, ball_dist()))
		$"Collision_bola".set_position(Vector2(0, ball_dist()))
		$"Collision_bola".get_shape().set_radius(bola.get_radius()*10)
		return true

func _on_Timer_timeout():
	$"Collision_bola".disabled = false
	var nova_bola = preload("res://bolainicial/bolainicial.tscn")
	#preload retorna uma PackedScene
	#quando uso instance(), retorno o root
	bola = nova_bola.instance()
	bola.set_position(Vector2(0, ball_dist() ))

	$"Collision_bola".set_position(Vector2(0, ball_dist()))
	$"Collision_bola".get_shape().set_radius(bola.get_radius()*10)

	self.add_child(bola)
	has_ball = true

#Morte do jogador
func _die():
	emit_signal("died")
	queue_free()

#Dano
func _toma_dano(valor):
	vida -= valor
	if vida <= 0:
		vida = 0
		_die()
	else:

		timer.start()
#Cura
func _cura(valor):
	vida = vida + valor
	if vida > MAX_HEALTH:
		vida = MAX_HEALTH
