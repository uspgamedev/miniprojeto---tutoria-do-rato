extends KinematicBody2D

var current_scale = get_scale()
const size = Vector2(0.1,0.1)

const RADIUS = 11
const MAX_SPEED = 200
const MAX_BOUNCES = 3

var launched = false
var hits = 0
var speed = 300
var direction

func get_radius():
	return RADIUS*get_scale().x/0.1

func launch_ball(dir):
	direction = dir.normalized()
	launched = true
	$CollisionShape2D.disabled = false

func _physics_process(delta):
	if(launched == true):
		var velocity = direction * speed
		var collision = move_and_collide(velocity * delta)
		
		if(collision):
			direction = direction - 2*(direction.dot(collision.normal))*collision.normal
			direction = direction.normalized()
			print("colidiu")
			hits += 1
		
		if(hits == MAX_BOUNCES):
			self.queue_free()

#Função que cresce a bola a partir de um Vector2
func cresce_bola(item):
	current_scale += item

	if current_scale > Vector2(1,1):
		current_scale = Vector2(1,1)

	set_scale(current_scale)

	return current_scale

#Função que diminui a bola a partir de um Vector2
func diminui_bola(item):
	current_scale -= item

	if current_scale < size:
		current_scale = size

	set_scale(current_scale)
	
	return current_scale

#Função que volta a bola para seu tamanho original
func bola_original():
	set_scale(size)
	current_scale = size

#tamanho é o tamanho do entulho
func _entulho_pego(tamanho):
	var add_scale

	if(tamanho == 0):
		add_scale = Vector2(0.1, 0.1)
	elif(tamanho == 1):
		add_scale = Vector2(0.2, 0.2)
	elif(tamanho == 2):
		add_scale = Vector2(0.3, 0.3)
	else:
		return false

	if(current_scale == Vector2(1,1)):
		return false

	else:
		if(current_scale + add_scale > Vector2(1,1)):
			current_scale = Vector2(1,1)
			set_scale(current_scale)
			return true

		else:
			current_scale += add_scale
			set_scale(current_scale)
			return true
