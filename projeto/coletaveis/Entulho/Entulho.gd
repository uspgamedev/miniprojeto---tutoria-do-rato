extends Node2D

var tamanho
enum {smol, medium, big}

func _on_COLETAVEL_is_collected(quem):
	#Sabemos que quem pegou o entulho
	#foi um player, que deve ter essa
	#função
	if(quem._pegou_entulho(tamanho)):
		get_owner().queue_free()
	print("entulho coletado")

func _ready():
	#tamanho aleatório do entulho
	randomize()
	tamanho = randi()%3
	
	if(tamanho == smol):
		get_owner().set_scale(Vector2(0.5, 0.5))
	elif(tamanho == medium):
		get_owner().set_scale(Vector2(1, 1))
	elif(tamanho == big):
		get_owner().set_scale(Vector2(1.5, 1.5))
