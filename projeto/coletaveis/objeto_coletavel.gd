extends Area2D

signal is_collected(quem) #quem coletou

func _on_AREA2d_body_entered(body):
	if body.is_in_group("Player"):
		emit_signal("is_collected", body)
		#queue_free()
