extends Node2D

var empty_spawners 
var objects_spawned = 0
var spawners_instanced

func _ready():
	randomize()
	#só recebo os child nodes em ready
	#porque tenho que esperar eles serem carregados
	empty_spawners = self.get_children()
	empty_spawners.pop_front()
	#removo o primeiro child node de empty_spawners
	#pois é o timer
	spawners_instanced = empty_spawners.size()
	print(spawners_instanced)
	
	for i in empty_spawners:
		i.connect("can_spawn", self, "when_collected")

func spawn_somewhere():
	#escolhendo o spawn aleatório
	empty_spawners.shuffle()

	objects_spawned += 1
	print(empty_spawners[0])
	empty_spawners[0].spawn()
	empty_spawners.remove(0)

func _on_Timer_timeout():
	if(objects_spawned < spawners_instanced):
		spawn_somewhere()

func when_collected(source):
	objects_spawned -= 1
	empty_spawners.push_back(source)

