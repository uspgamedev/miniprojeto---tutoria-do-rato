extends Node2D

signal can_spawn(source)

onready var object = preload("res://coletaveis/objeto_coletavel.tscn")
onready var timer = Timer.new()

func _ready():
	timer.set_wait_time(4) 
	timer.connect("timeout", self, "_on_timer_timeout")
	add_child(timer)
	#spawn()

func spawn():
	var new_object = object.instance()
	new_object.connect("is_collected", self, "coletavel_collected")
	#a linha acima foi usada para permitir a utilização do sinal da instanced scene
	add_child(new_object)
	timer.stop()

func _on_timer_timeout():
	spawn()

func coletavel_collected():
	timer.start()
	emit_signal("can_spawn", self)