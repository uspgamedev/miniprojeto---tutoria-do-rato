extends KinematicBody2D

#Constantes
const GRAVITY = 400
const TERMINAL_SPEED = 2000
const SPEED = 300

#Contagem do pulo duplo
var jump_count = 0

#Estado do personagem
enum direction {none,left,right}
var state = direction.none

#Atributos
var move_direction = Vector2(0,0)

func _input(event):

	#Actions Pressed
	if event.is_action_pressed("ui_up"):
		if jump_count < 1:
			move_direction.y = -2000
			jump_count += 1

	elif event.is_action_pressed("ui_down"):
		pass

	elif event.is_action_pressed("ui_left"):
		state = direction.left

	elif event.is_action_pressed("ui_right"):
		state = direction.right

	#Actions Released
	elif event.is_action_released("ui_up"):
		pass

	elif event.is_action_released("ui_down"):
		pass

	elif event.is_action_released("ui_left") and state == direction.left:
		state = direction.none

	elif event.is_action_released("ui_right") and state == direction.right:
		state = direction.none

func _physics_process(delta):
	#Aplica força da gravidade
	move_direction.y += GRAVITY

	#Limita a velocidade de queda
	if move_direction.y > TERMINAL_SPEED:
		move_direction.y = TERMINAL_SPEED

	#Lidar com os estados
	if state == direction.left:
		move_direction.x = -SPEED

	elif state == direction.right:
		move_direction.x = SPEED

	else:
		move_direction.x = 0

	#Reseta a contagem do pulo duplo
	if is_on_floor():
		jump_count = 0
	
	#Move o personagem
	move_and_slide(move_direction, Vector2(0,-1))
