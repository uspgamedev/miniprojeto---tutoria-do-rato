extends KinematicBody2D

#Constantes
const MOVE_SPEED = 400
const ROTATION_SPEED = 2

#Variaveis
var direction = Vector2(1,0)

#Note que aqui eu usei tanto o process quanto o physics_process
#A diferença entre elese é que o physics é chamado com um delta
#mais o menos constante. Em geral, usamos o physics para a fisica
#ou coisas que necessitam de precisão, mas não há (muito) problema
#em colocar tudo no physics por simplicidade. 

func _process(delta):
	direction = direction.rotated(ROTATION_SPEED*delta)

func _physics_process(delta):
	move_and_slide(direction*MOVE_SPEED)
